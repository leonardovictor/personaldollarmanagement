package com.example.personaldollarmanagement.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.personaldollarmanagement.R;
import com.example.personaldollarmanagement.activities.AdicaoDeRegistrosActivity;
import com.example.personaldollarmanagement.data.DAORegistroSingleton;

public class MainActivity extends AppCompatActivity {

    private String saldo;
    private TextView txtSaldo;
    private TextView txtUltimoGasto;
    private TextView txtUltimoLucro;
    private TextView txtCatMaisCara;
    private TextView txtCatMaisBarata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.txtSaldo = findViewById(R.id.textViewSaldoTotal);
        this.txtUltimoGasto = findViewById(R.id.textViewUltimoGasto);
        this.txtUltimoLucro = findViewById(R.id.textViewUltimoLucro);
        this.txtCatMaisCara = findViewById(R.id.textViewCatMaisCara);
        this.txtCatMaisBarata = findViewById(R.id.textViewCatMaisBarata);

        refresh();
    }

    @Override
    protected void onResume() {
        refresh();
        super.onResume();
    }

    public void onClickAdicaoRegistros(View v){
        Intent adicaoRegistros = new Intent(this, AdicaoDeRegistrosActivity.class);
        startActivity(adicaoRegistros);
    }

    public void onClickVisualizacaoRegistros(View v){
        Intent visualizacaoRegistros = new Intent(this, VisualizacaoDeRegistrosActivity.class);
        startActivity(visualizacaoRegistros);
    }

    public void refresh(){
        this.saldo = DAORegistroSingleton.getINSTANCE().getSaldo();
        this.txtSaldo.setText(saldo);

        this.txtUltimoGasto.setText(DAORegistroSingleton.getINSTANCE().ultimoGasto());
        this.txtUltimoLucro.setText(DAORegistroSingleton.getINSTANCE().ultimoLucro());
        this.txtCatMaisBarata.setText(DAORegistroSingleton.getINSTANCE().categoriaMaisBarata());
        this.txtCatMaisCara.setText(DAORegistroSingleton.getINSTANCE().categoriaMaisCara());

    }

}