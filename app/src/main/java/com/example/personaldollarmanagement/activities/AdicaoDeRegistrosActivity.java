package com.example.personaldollarmanagement.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.database.sqlite.SQLiteBlobTooBigException;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.personaldollarmanagement.R;
import com.example.personaldollarmanagement.data.DAORegistroSingleton;
import com.example.personaldollarmanagement.model.Registro;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

public class AdicaoDeRegistrosActivity extends AppCompatActivity {

    private Spinner spinner;
    private EditText edtQuantia;
    private EditText edtData;
    private EditText edtDescricao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adicao_de_registros);

        spinner = findViewById(R.id.spinner);
        edtQuantia = findViewById(R.id.editTextQuantia);
        edtData = findViewById(R.id.editTextData);
        edtDescricao = findViewById(R.id.editTextDescricao);
    }

    public void onClickData(View view){

        String pattern = "dd/MM/yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String data = simpleDateFormat.format(new Date());
        edtData.setText(data);
    }

    public void onClickAdicionar(View view){

        // adiciona o registro
        String categoria = spinner.getSelectedItem().toString();
        BigDecimal quantia = new BigDecimal(edtQuantia.getText().toString());
        String data = edtData.getText().toString();
        String desc = edtDescricao.getText().toString();

        if ( !categoria.isEmpty() && !quantia.toString().isEmpty() && !data.isEmpty() && !desc.isEmpty()){
            Registro registro = new Registro(categoria, quantia, data, desc);
            DAORegistroSingleton.getINSTANCE().addRegistro(registro);
            Toast.makeText(this, getString(R.string.registro_criado), Toast.LENGTH_SHORT).show();
            // Volta para a main
            finish();
            return;
        }
        Toast.makeText(this, getString(R.string.registro_nao_criado), Toast.LENGTH_LONG).show();

    }
}