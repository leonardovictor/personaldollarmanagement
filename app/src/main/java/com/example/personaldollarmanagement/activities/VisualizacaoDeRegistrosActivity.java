package com.example.personaldollarmanagement.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.personaldollarmanagement.R;
import com.example.personaldollarmanagement.adapter.RegistroAdapter;
import com.example.personaldollarmanagement.data.DAORegistroSingleton;
import com.example.personaldollarmanagement.model.Registro;

import java.math.BigDecimal;

public class VisualizacaoDeRegistrosActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizacao_de_registros);

        recyclerView = findViewById(R.id.recyclerView);

        // layout
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager( layoutManager );

        // adapter
        RegistroAdapter adapter = new RegistroAdapter();
        recyclerView.setAdapter( adapter );
    }

}