package com.example.personaldollarmanagement.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.math.BigDecimal;

public class Registro implements Parcelable {

    private String categoria;
    private BigDecimal quantia;
    private String data;
    private String descricao;

    public Registro(String categoria, BigDecimal quantia, String data, String descricao) {
        this.categoria = categoria;
        this.quantia = quantia;
        this.data = data;
        this.descricao = descricao;
    }

    public String getCategoria() {
        return categoria;
    }

    public BigDecimal getQuantia() {
        return quantia;
    }

    public String getData() {
        return data;
    }

    public String getDescricao() {
        return descricao;
    }

    protected Registro(Parcel in) {
        categoria = in.readString();
        data = in.readString();
        descricao = in.readString();
    }

    public static final Creator<Registro> CREATOR = new Creator<Registro>() {
        @Override
        public Registro createFromParcel(Parcel in) {
            return new Registro(in);
        }

        @Override
        public Registro[] newArray(int size) {
            return new Registro[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(categoria);
        dest.writeString(data);
        dest.writeString(descricao);
    }
}
