package com.example.personaldollarmanagement.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.personaldollarmanagement.R;
import com.example.personaldollarmanagement.data.DAORegistroSingleton;
import com.example.personaldollarmanagement.model.Registro;

public class RegistroAdapter extends RecyclerView.Adapter<RegistroAdapter.MyViewHolder> implements View.OnClickListener{

    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_registro, parent, false);

        return new MyViewHolder(item);
    }

    @Override
    public void onBindViewHolder(RegistroAdapter.MyViewHolder holder, int position) {

        Registro registro = DAORegistroSingleton.getINSTANCE().getRegistros().get(position);

        holder.txtCategoria.setText(registro.getCategoria());
        holder.txtQuantia.setText("$" + registro.getQuantia().toString());
        holder.txtData.setText(registro.getData());
        holder.txtDescricao.setText(registro.getDescricao());
    }

    @Override
    public int getItemCount() {
        return DAORegistroSingleton.getINSTANCE().getRegistros().size();
    }

    @Override
    public void onClick(View v) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txtCategoria;
        private TextView txtQuantia;
        private TextView txtData;
        private TextView txtDescricao;

        public MyViewHolder(View itemView) {
            super(itemView);

            txtCategoria = itemView.findViewById(R.id.textCategoria);
            txtQuantia = itemView.findViewById(R.id.textQuantia);
            txtData = itemView.findViewById(R.id.textData);
            txtDescricao = itemView.findViewById(R.id.textDescricao);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int visibilidade = txtDescricao.getVisibility();

                    if (visibilidade != View.VISIBLE){
                        txtDescricao.setVisibility(View.VISIBLE);
                        return;
                    }
                    txtDescricao.setVisibility(View.GONE);
                }
            });
        }

    }
}
