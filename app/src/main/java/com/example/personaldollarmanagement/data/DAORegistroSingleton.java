package com.example.personaldollarmanagement.data;

import android.util.Log;

import com.example.personaldollarmanagement.activities.MainActivity;
import com.example.personaldollarmanagement.model.Registro;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DAORegistroSingleton {
    private static DAORegistroSingleton INSTANCE;
    private List<Registro> registros;

    private DAORegistroSingleton(){
        this.registros = new ArrayList<>();
    };

    public static DAORegistroSingleton getINSTANCE(){
        if ( INSTANCE == null ){
            INSTANCE = new DAORegistroSingleton();
        }
        return INSTANCE;
    }

    public List<Registro> getRegistros(){
        return this.registros;
    }

    public void addRegistro(Registro registro){
        this.registros.add(registro);
    }

    public String getSaldo(){

        BigDecimal saldo = new BigDecimal("0");

        if (!this.registros.isEmpty()){

            for (Registro item: this.registros) {

                if ( !item.getCategoria().contentEquals("Entrada de dinheiro")){
                    saldo = saldo.subtract(item.getQuantia());
                } else {
                    saldo = saldo.add(item.getQuantia());
                }

            }
        }

        String saldoStr;
        int compare = saldo.compareTo( new BigDecimal("0"));
        if ( compare < 0 ){
            return saldoStr = "-$" + saldo.abs().toString();
        } else if ( compare > 0){
            return saldoStr = "+$" + saldo.abs().toString();
        }

        return "$" + saldo.toString();
    }

    public String ultimoGasto (){

        // Caso não tenha nenhum elemento
        String ultimoGasto = "---";

        // // Retorna "nenhum" caso não tenha elementos
        if (this.registros.isEmpty()){
            return ultimoGasto;
        }

       for ( int lastPos = this.registros.size() - 1; lastPos >= 0; lastPos--){
           if ( !this.registros.get( lastPos ).getCategoria().contentEquals("Entrada de dinheiro")){
               return this.registros.get( lastPos ).getCategoria();
           }
       }

        return ultimoGasto;
    }

    public String ultimoLucro (){
        // Caso não tenha nenhum elemento
        String ultimoLucro = "0";

        // // Retorna "nenhum" caso não tenha elementos
        if (this.registros.isEmpty()){
            return "$" + ultimoLucro;
        }

        for ( int lastPos = this.registros.size() - 1; lastPos >= 0; lastPos--){
            if ( this.registros.get( lastPos ).getCategoria().contentEquals("Entrada de dinheiro")){
                return "$" + this.registros.get( lastPos ).getQuantia().toString();
            }
        }

        return "$" + ultimoLucro;
    }

    public String categoriaMaisCara (){

        String categoria = "---";
        BigDecimal totalAlimentacao = new BigDecimal("0");
        BigDecimal totalComunicação = new BigDecimal("0");
        BigDecimal totalSaude = new BigDecimal("0");
        BigDecimal totalTransporte = new BigDecimal("0");
        BigDecimal totalVestuário = new BigDecimal("0");
        BigDecimal totalLazer = new BigDecimal("0");

        // Calcular o valor total de cada categoria
        for ( int i = 0; i < this.registros.size(); i++){

            if (this.registros.get( i ).getCategoria().contentEquals("Alimentação")){
                totalAlimentacao = totalAlimentacao.add(this.registros.get(i).getQuantia());
            }
            if (this.registros.get( i ).getCategoria().contentEquals("Comunicação")){
                totalComunicação = totalComunicação.add(this.registros.get(i).getQuantia());
            }
            if (this.registros.get( i ).getCategoria().contentEquals("Saúde")){
                totalSaude = totalSaude.add(this.registros.get(i).getQuantia());
            }
            if (this.registros.get( i ).getCategoria().contentEquals("Transporte")){
                totalTransporte = totalTransporte.add(this.registros.get(i).getQuantia());
            }
            if (this.registros.get( i ).getCategoria().contentEquals("Vestuário")){
                totalVestuário = totalVestuário.add(this.registros.get(i).getQuantia());
            }
            if (this.registros.get( i ).getCategoria().contentEquals("Lazer")){
                totalLazer = totalLazer.add(this.registros.get(i).getQuantia());
            }

        }

        // Cria uma lista chave e valor
        Map<String, BigDecimal> map = new HashMap<>();
        map.put("Alimentação", totalAlimentacao);
        map.put("Comunicação", totalComunicação);
        map.put("Saúde", totalSaude);
        map.put("Transporte", totalTransporte);
        map.put("Vestuário", totalVestuário);
        map.put("Lazer", totalLazer);

        // Ordena a lista
        List<Map.Entry<String, BigDecimal>> list = new ArrayList<>(map.entrySet());
        list.sort(Map.Entry.comparingByValue());

        // Verifica se o maior valor existe para caso a lista não contenha valor algum
        int verificaLista = list.get(list.size() - 1).getValue().compareTo( new BigDecimal("0"));

        if ( verificaLista == 0 ){
            return categoria;
        }

        // Retorna a categoria com o maior gasto
        categoria = list.get(list.size() - 1).getKey();

        return categoria;
    }

    public String categoriaMaisBarata (){

        String categoria = "---";
        BigDecimal totalAlimentacao = new BigDecimal("0");
        BigDecimal totalComunicação = new BigDecimal("0");
        BigDecimal totalSaude = new BigDecimal("0");
        BigDecimal totalTransporte = new BigDecimal("0");
        BigDecimal totalVestuário = new BigDecimal("0");
        BigDecimal totalLazer = new BigDecimal("0");

        // Calcular o valor total de cada categoria
        for ( int i = 0; i < this.registros.size(); i++){

            if (this.registros.get( i ).getCategoria().contentEquals("Alimentação")){
                totalAlimentacao = totalAlimentacao.add(this.registros.get(i).getQuantia());
            }
            if (this.registros.get( i ).getCategoria().contentEquals("Comunicação")){
                totalComunicação = totalComunicação.add(this.registros.get(i).getQuantia());
            }
            if (this.registros.get( i ).getCategoria().contentEquals("Saúde")){
                totalSaude = totalSaude.add(this.registros.get(i).getQuantia());
            }
            if (this.registros.get( i ).getCategoria().contentEquals("Transporte")){
                totalTransporte = totalTransporte.add(this.registros.get(i).getQuantia());
            }
            if (this.registros.get( i ).getCategoria().contentEquals("Vestuário")){
                totalVestuário = totalVestuário.add(this.registros.get(i).getQuantia());
            }
            if (this.registros.get( i ).getCategoria().contentEquals("Lazer")){
                totalLazer = totalLazer.add(this.registros.get(i).getQuantia());
            }

        }

        // Cria uma lista chave e valor
        Map<String, BigDecimal> map = new HashMap<>();
        map.put("Alimentação", totalAlimentacao);
        map.put("Comunicação", totalComunicação);
        map.put("Saúde", totalSaude);
        map.put("Transporte", totalTransporte);
        map.put("Vestuário", totalVestuário);
        map.put("Lazer", totalLazer);

        // Ordena a lista
        List<Map.Entry<String, BigDecimal>> list = new ArrayList<>(map.entrySet());
        list.sort(Map.Entry.comparingByValue());

        // Verifica se exite algum valor além de zero
        int verificaTotalAlimentacao = list.get(0).getValue().compareTo( new BigDecimal("0"));
        int verificaTotalComunicação = list.get(1).getValue().compareTo( new BigDecimal("0"));
        int verificaTotalSaude = list.get(2).getValue().compareTo( new BigDecimal("0"));
        int verificaTotalTransporte = list.get(3).getValue().compareTo( new BigDecimal("0"));
        int verificaTotalVestuário = list.get(4).getValue().compareTo( new BigDecimal("0"));
        int verificaTotalLazer = list.get(5).getValue().compareTo( new BigDecimal("0"));

        if ( verificaTotalAlimentacao == 0 && verificaTotalComunicação == 0
                && verificaTotalSaude == 0 && verificaTotalTransporte == 0
                && verificaTotalVestuário == 0 && verificaTotalLazer == 0){
            // Retorna os "---"
            return categoria;
        }

        for (Map.Entry<String, BigDecimal> item : list ) {
            int compara = item.getValue().compareTo( new BigDecimal("0"));
            if ( compara == 1){
                return item.getKey();
            }
        }

        // Retorna a categoria com menor gasto
        categoria = list.get(0).getKey();

        return categoria;

    }
}
